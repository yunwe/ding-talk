FROM python:3.12-slim
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN useradd dingtalk
WORKDIR /ding-talk/
COPY app.py config.ini  dockerfile LICENSE README.md requirements.txt /ding-talk/
COPY ding /ding-talk/ding
RUN pip3 install --no-cache-dir -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt && \
    rm -rf /root/.cache/pip
RUN chown -R dingtalk:dingtalk /ding-talk/
USER dingtalk
EXPOSE 5000
CMD ["python","app.py"]
