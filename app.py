from ding import create_app
from gevent import pywsgi

# master
app = create_app()
server = pywsgi.WSGIServer(('0.0.0.0', 5000), app)
server.serve_forever()
